<?php

use yii\db\Migration;

class m161025_173947_create_pay extends Migration
{
    public function up()
    {
        $this->createTable('pay',[
            'id'=>$this->primaryKey(),
            'user_id'=>$this->integer(),
            'payment_id'=>$this->string(),
            'created_at'=>$this->integer(),
            'updated_at'=>$this->integer(),
        ]);
    }

    public function down()
    {
        $this->dropTable('pay');
    }
}
