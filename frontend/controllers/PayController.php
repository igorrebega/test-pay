<?php

namespace frontend\controllers;

use common\components\Paypal;
use common\models\Pay;
use yii\filters\AccessControl;

class PayController extends \yii\web\Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Pay with PayPal
     * @return \yii\web\Response
     */
    public function actionPay()
    {
        $PayPal = new Paypal();
        $response = $PayPal->testPayment(10, 'test');
        $url = $response->links[1]->href;

        return $this->redirect($url);
    }

    /**
     * Show payment button
     * @return string
     */
    public function actionPayUrl()
    {
        $pay = Pay::findOne(['user_id' => \Yii::$app->user->id]);
        if ($pay) {
            return \Yii::$app->response->sendFile("file.zip");
        }
        return $this->render('pay-url.php');
    }

    /**
     * Check Payment
     * @param $payerID
     * @return $this|string
     */
    public function actionCheck($PayerID, $paymentId)
    {
        $payPal = new Paypal();
        if ($payPal->checkPay($PayerID, $paymentId)) {
            return \Yii::$app->response->sendFile("file.zip");
        }
        return $this->render('index');
    }

}
