<?php
namespace frontend\controllers;

use common\components\Paypal;
use DTS\eBaySDK\Sdk;
use DTS\eBaySDK\Trading\Services\TradingService;
use DTS\eBaySDK\Trading\Types\ApplicationDeliveryPreferencesType;
use DTS\eBaySDK\Trading\Types\DeliveryURLDetailType;
use DTS\eBaySDK\Trading\Types\NotificationEnableArrayType;
use DTS\eBaySDK\Trading\Types\NotificationEnableType;
use DTS\eBaySDK\Trading\Types\SetNotificationPreferencesRequestType;
use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $trading = new TradingService([
            'siteId' => 3,
            'sandbox' => true,
            'credentials' => [
                'appId' => 'IgorRebe-TestApi-SBX-dbff6a2f3-113c2997',
                'certId' => 'SBX-bff6a2f3487c-d90a-49ce-a92d-90bd',
                'devId' => 'c9eb9ad2-f991-4bc8-9bab-e9b3b80a7e60'
            ]
        ]);
        $ApplicationDeliveryPreferences = new ApplicationDeliveryPreferencesType([
            'AlertEmail' => 'mailto:igorrebega@gmail.com',
            'AlertEnable' => 'Enable',
            'ApplicationEnable' => 'Disable',
            'ApplicationURL' => 'mailto:igorrebega@gmail.com',

        ]);

        $request = new SetNotificationPreferencesRequestType([
            'ApplicationDeliveryPreferences' => $ApplicationDeliveryPreferences,
            'RequesterCredentials' => [
                'eBayAuthToken' => 'AgAAAA**AQAAAA**aAAAAA**FWgTWA**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4GjD5iFpQmdj6x9nY+seQ**DPsDAA**AAMAAA**Eq4oclNOQaYVziAE8ZAEb3ZKOGcnT75SfUFQKCLcPhd5jklFul3g2BzNbrhBpWmmv2VEqghidBK1CdHOnbfh7XpzUZVBqRLHU+sz916/6Zr/yIUSv2lM4t4zAUe1j+BXYmvrJG7+hRHuTdIn1ujelJAeTmt8iVFIgwCrIT1kcUBt9khcex1xozHKJC5xhZMDlebf9N4AHnu1ZeFNtfOmyTe4KERNlEmLcVb4gjN5ljrY4y2IXNdJqoHPybw/a49FYXDORIEeEQVcvDMNJck8NpxchPYyP7nSO2n7If6fCZ9Mi/0do78V1FrS1+u5vRRdWABXczFtNm9jwF5sAgg9bfSswQNacE3JzCv94QnxLRhwibFB/ZQsxpzgeQXELzxXAIY970pCDv9HbMYDB5C1Ue3ODkQ4TYWeQtVAG4t79Mnuj9J2V0Opj0t4WDxO6kQVILmtOUDyXjHEtuRfejxsSsnsvQb/DsCZt0erIL70O7pj6De23ixvt6ETcekvbmWneUQW5B0kSB3zygZQrdf+rDjn8e4RDoB+TFUs+9y06mz61X2aVF2Go8GyyluAopkBA0bwMYydLpxFHpEa0yqzlr1a28CnUkKvS++mB5em2v7l1z+NJ1kP1urO9xbulXQV9tNOPOPWMJtJKKVQ2Q+T01NQpVdS6OtQMXZHAFKXDnWb9TWuniYMAJm/S4jSTsy27JpkuX8+ZWxOZnfJxpUNCZ4WGhW0As4GG9noKj2eYZ9mAhkW4uRDq3WV1vZl/q71',

            ]
        ]);
        print_r($trading->setNotificationPreferences($request));

        return $this->render('index');
    }


    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(['/pay/pay-url']);
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
}
