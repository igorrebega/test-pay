<?php
namespace common\components;

use common\models\Pay;
use PayPal\Api\PaymentExecution;
use Yii;
use yii\base\Exception;
use yii\base\Component;
use PayPal\Api\Amount;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\Transaction;
use PayPal\Api\RedirectUrls;
use yii\helpers\Url;

class Paypal extends Component
{
    public $apiContext;

    public function __construct()
    {
        $this->apiContext = new \PayPal\Rest\ApiContext(
            new \PayPal\Auth\OAuthTokenCredential(
                Yii::$app->params['paypal.client_id'],
                Yii::$app->params['paypal.secret_id']
            )
        );
        parent::__construct();
    }

    /**
     * Do payment with PayPal
     * @param $price
     * @param $description
     * @return Payment
     */
    public function testPayment($price, $description)
    {
        $payer = new Payer();
        $payer->setPaymentMethod("paypal");


        $amount = new Amount();
        $amount->setCurrency("USD")->setTotal($price);

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setDescription($description)
            ->setInvoiceNumber(uniqid());

        $redirectUrls = new RedirectUrls();
        $redirectUrls->setReturnUrl(Url::to(['/pay/check', 'user_id' => Yii::$app->user->id], true))
            ->setCancelUrl(Url::to(['/pay/false', 'user_id' => Yii::$app->user->id], true));

        $payment = new Payment();
        $payment->setIntent("sale")
            ->setPayer($payer)
            ->setRedirectUrls($redirectUrls)
            ->setTransactions(array($transaction));

        $request = clone $payment;

        try {
            $payment->create($this->apiContext);
        } catch (Exception $ex) {
            \ResultPrinter::printError("Created Payment Using PayPal. Please visit the URL to Approve.", "Payment", null, $request, $ex);
            exit(1);
        }

        return $payment;
    }

    /**
     * Check if payment is valid
     * @param $payer_id
     * @return bool
     */
    public function checkPay($payer_id,$payment_id)
    {
        $paymentExecution = new PaymentExecution();
        $paymentExecution->payer_id = $payer_id;
        $payment = new Payment();
        $payment->id = $payment_id;
        if($payment->execute($paymentExecution,$this->apiContext) && $payment->state == 'approved'){
            $pay = new Pay();
            $pay->payment_id = $payment_id;
            $pay->user_id = Yii::$app->user->id;
            $pay->save();

            return true;
        }
        return false;
    }

}